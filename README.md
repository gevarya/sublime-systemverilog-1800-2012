# Sublime-SystemVerilog-1800-2012
* A hobby project of mine (Gergo VEKONY) to get all of the syntax covered in a nice fashion
* My goal was to have a good and fully functional syntax definition with **correct and exact capture group identification**. (See screenshot below.)

## Current state ##
* Supports SV-1800-2012 LRM's Clause 1-26 with an exception of Clause 17 ('checkers') and Clause 24 ('programs') 
* Parenthesis, brackets, braces matching
* Structure opening and closing keywords matching
* Orphaned keywords highlight
* Partial auto-indentation support
* Naming conventions are supported, eg.: (e)num, (if)interface, (m)ember, (mp)modport, (o)utput, (q)register, (t)ype, (vif)virtual interface
* Allmost full UVM support
* Full support for the Nilium2-extended theme (my other repo here)

## Future plans ##
* Review the capture group names and structure
* Add more guard expressions to increase process speed
* Finishing Clause 17, 24, 26 and the rest
* Finishing UVM support
* Creating a version for the vanilla textmate capture group support

## Screenshot with Nilium2-extended-Tubnil theme ##
Work in progress image using www.cluelogic.com jelly bean factory example code: (Autoindented version, no manual correction. ;])

![2016_12_31.PNG](https://bitbucket.org/repo/oaEbd8/images/625143169-2016_12_31.PNG)